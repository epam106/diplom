# Create VM for pipeline IaC

```

```

# Azure Create Resorce Group for kube

```
/media/vlad/work/epam/azure_resource_group

terraform plan
terraform apply
terraform destroy
```

# Создание disk для хранения БД

```
az disk create \
 --resource-group MC_azure-k8stest_k8stest_westus \
 --name myAKSDisk \
 --size-gb 4 \
 --sku Standard_LRS \
 --query id --output tsv

/subscriptions/1bb659ca-bfc3-49df-96bb-12db71c5fe9f/resourceGroups/MC_azure-k8stest_k8stest_westus/providers/Microsoft.Compute/disks/myAKSDisk
```

# Azure Kubernetes Cluster

/media/vlad/work/epam/azure_k

```
terraform plan -out out.plan
terraform apply out.plan
terraform destroy

az account set --subscription 1bb659ca-bfc3-49df-96bb-12db71c5fe9f
az aks get-credentials --resource-group azure-k8stest --name k8stest
```

# Helm gitlab-runner

/media/vlad/work/epam/helm/gitlab-runner

```
k apply -f namespaceMF.yml
helm install --namespace mf gitlab-runner -f values.yml gitlab/gitlab-runner
```

# Gitlab registry AKS

1. добавить имя кластера
2. добавить ссылка на кластер
3. добавить client-sertificat | base64 --decode
4. токен
