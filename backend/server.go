package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"fmt"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Wind struct {
	SourceId  string    `json:"sourceId"`
	ValidFrom time.Time `json:"validFrom"`
}

type Result struct {
	Duration float64
	WindObs  []*WindObs
}

type WindObs struct {
	SourceId      string      `json:"sourceId"`
	ReferenceTime time.Time   `json:"referenceTime"`
	Observation   []WindValue `json:"observations"`
}

type WindValue struct {
	Value float64 `json:"value"`
}

func main() {

	// var loc []*Wind

	// Echo instance

	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.CORS())
	// e.Static("/", "/public")
	e.Use(middleware.Static("public"))

	time.Sleep(60000 * time.Millisecond)
	db, err := connectDB()
	if err != nil {
		fmt.Println(err, " :   failed to connect database")
	}
	// Migrate the schema
	db.AutoMigrate(&Wind{})

	e.GET("/api/wind/update", func(c echo.Context) error {

		start := time.Now()
		// Код для измерения

		fmt.Println("/api/wind/update")

		updateDB(&db)

		duration := time.Since(start)

		return c.JSON(http.StatusOK, duration.Seconds())

	})

	e.GET("/api/wind/kube", func(c echo.Context) error {

		fmt.Println("/kube")

		return c.JSON(http.StatusOK, "Hello Kubernetes")

	})

	e.GET("/api/wind/load", func(c echo.Context) error {

		fmt.Println("/load")

		load()

		return c.JSON(http.StatusOK, "OK")

	})

	e.GET("/api/wind/pullPSQL", func(c echo.Context) error {

		var res Result
		start := time.Now()
		// Код для измерения

		fmt.Println("/pullPSQL")
		res.WindObs = apiObs(&db)

		duration := time.Since(start)
		res.Duration = duration.Seconds()

		return c.JSON(http.StatusOK, res)

	})

	// Start server
	e.Logger.Fatal(e.Start(":1320"))

}

func apiReq(request string) []byte {

	client := &http.Client{}
	req, _ := http.NewRequest("GET", request, nil)
	req.SetBasicAuth("d4b190bb-d3c3-45f3-856e-25f4a44ec2e4", "36982ab0-2bfa-42d9-b3e4-9757b58cfaf2")
	resp, err := client.Do(req)

	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}

	return body
}

func apiWind() []*Wind {

	req := "https://frost.met.no/observations/availableTimeSeries/v0.jsonld?elements=wind_from_direction&fields=sourceId%2C%20validFrom"
	body := apiReq(req)

	w := parse(body)

	return w
}

func apiObs(db *gorm.DB) []*WindObs {

	var wind []*Wind
	var w []*WindObs

	db.Raw("SELECT * FROM winds ORDER BY valid_from LIMIT 10").Scan(&wind)

	for _, v := range wind {

		req := "https://frost.met.no/observations/v0.jsonld?sources=" + v.SourceId + "&referencetime=" + v.ValidFrom.Format("2006-01-02") + "%2F" + v.ValidFrom.Add(24*time.Hour).Format("2006-01-02") + "&elements=wind_from_direction"
		fmt.Println(req)
		w = append(w, parseObs(apiReq(req))[0])
	}

	return w
}

func connectDB() (gorm.DB, error) {

	// loc := Wind{"start", time.Now()}
	dsn := "host=localhost user=admin password=admin dbname=diplom port=5432 sslmode=disable"
	db_g, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	// db_g.Exec("CREATE TABLE winds")
	fmt.Println("table winds create")

	return *db_g, err

}

func parse(api []byte) []*Wind {

	var w []*Wind
	var objmap map[string]*json.RawMessage

	err := json.Unmarshal(api, &objmap)
	if err != nil {
		fmt.Println("file proc", err)
	}
	err = json.Unmarshal(*objmap["data"], &w)
	if err != nil {
		fmt.Println("file proc", err)
	}

	return w

}

func parseObs(api []byte) []*WindObs {

	var w []*WindObs
	var objmap map[string]*json.RawMessage

	err := json.Unmarshal(api, &objmap)
	if err != nil {
		fmt.Println("file proc", err)
	}
	err = json.Unmarshal(*objmap["data"], &w)
	if err != nil {
		fmt.Println("file proc", err)
	}

	return w

}

func updateDB(db *gorm.DB) {

	loc := apiWind()
	fmt.Println("record table new data")

	// Delete table data
	db.Exec("DELETE FROM winds")

	// Create
	db.Create(&loc)

}

func load() {

	for i := 1; i < 100000; i++ {
		go factorial(i)
	}

	fmt.Println("The End")

}

func factorial(n int) {
	if n < 1 {
		fmt.Println("Invalid input number")
		return
	}
	result := 1
	for i := 1; i <= n; i++ {
		result *= i
	}
	fmt.Println(n, "-", result)
}
